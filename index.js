const path = require("path")
const fastify = require("fastify")({ logger: true })
const trx = require("./trx.js")
const database = require("./database.js")

fastify.register(require("@fastify/static"), {
	root: path.join(__dirname),
	serve: false,
})

fastify.get("/", async (request, reply) => {
	return reply.sendFile("index.html")
})

fastify.get("/api/a", async (request, reply) => {
	const data = await database(request.query.trx_id).readA()
	return { ok: true, data }
})

fastify.get("/api/b", async (request, reply) => {
	const data = await database(request.query.trx_id).readB()
	return { ok: true, data }
})

fastify.get("/api/c", async (request, reply) => {
	const data = await database(request.query.trx_id).readC()
	return { ok: true, data }
})

fastify.post("/api/a", async (request, reply) => {
	const ok = await database(request.body.trx_id).writeA(request.body.data)
	return { ok }
})

fastify.post("/api/b", async (request, reply) => {
	const ok = await database(request.body.trx_id).writeB(request.body.data)
	return { ok }
})

fastify.post("/api/c", async (request, reply) => {
	const ok = await database(request.body.trx_id).writeC(request.body.data)
	return { ok }
})

fastify.post("/api/begin", async (request, reply) => {
	const trx_id = await trx.begin()

	return { ok: true, data: trx_id }
})

fastify.post("/api/commit", async (request, reply) => {
	try {
		const success = await trx.commit(request.body.trx_id)
		if (!success) {
			console.warn("Invalid trx_id")
		}
		return { ok: success }
	} catch (err) {
		console.error("commit error", err)
		return { ok: false }
	}
})

const start = async () => {
	try {
		await fastify.listen({ port: 3000 })
	} catch (err) {
		fastify.log.error(err)
		process.exit(1)
	}
}

start()
