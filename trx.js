const { randomUUID } = require("crypto")
const pg = require("knex")({
	client: "pg",
	connection: { user: "postgres", database: "trx_example" }
})

const transactions = new Map()
const timeout = 5000

const begin = async () => {
	const trx_id = randomUUID()
	const trx = await pg.transaction()
	const timer = setTimeout(() => {
		trx.rollback()
		clean()
	}, timeout)
	const clean = () => {
		clearTimeout(timer)
		transactions.delete(trx_id)
	}
	transactions.set(trx_id, { trx, clean })
	return trx_id
}

const commit = async (trx_id) => {
	const transaction = transactions.get(trx_id)
	if (transaction) {
		const promise = transaction.trx.commit()
		promise.finally(() => {
			transaction.clean()
		})
		await promise
		return true
	} else {
		return false
	}
}

const use = (trx_id) => {
	return transactions.get(trx_id)?.trx
}

module.exports = { pg, begin, commit, use }
