const trx = require("./trx.js")

module.exports = (trx_id) => {
	const knex = trx_id ? trx.use(trx_id) : trx.pg
	if (!knex) throw new Error("Invalid trx_id")

	return {
		readA: async () => {
			return knex("a")
				.where("id", 1)
				.select("name")
				.then(rows => rows[0].name)
		},
		readB: async () => {
			return knex("b")
				.where("id", 1)
				.select("name")
				.then(rows => rows[0].name)
		},
		readC: async () => {
			return knex("c")
				.where("id", 1)
				.select("name")
				.then(rows => rows[0].name)
		},
		writeA: async (value) => {
			return knex("a")
				.where("id", 1)
				.update("name", value)
				.then(rows => true, err => false)
		},
		writeB: async (value) => {
			return knex("b")
				.where("id", 1)
				.update("name", value)
				.then(rows => true, err => false)
		},
		writeC: async (value) => {
			return knex("c")
				.where("id", 1)
				.update("name", value)
				.then(rows => true, err => false)
		},
	}
}
