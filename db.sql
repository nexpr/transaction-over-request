create database trx_example;
\c trx_example;

create table a (
    id integer primary key,
    name text unique
);
create table b (
    id integer primary key,
    name text unique
);
create table c (
    id integer primary key,
    name text unique
);

insert into a values (1, ''), (2, 'text');
insert into b values (1, ''), (2, 'text');
insert into c values (1, ''), (2, 'text');
